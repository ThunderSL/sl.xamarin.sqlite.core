﻿using Microsoft.EntityFrameworkCore;

namespace sl.xamarin.sqlite.core
{
    public sealed class DatabaseContext : DbContext
    {
        //public DbSet<Table> Table { get; set; }                           <----- Add your specific tables of the database

        //Filepath of database
        private readonly string _databasePath;

        /// <summary>
        /// Constructor of database context
        /// </summary>
        /// <param name="databasePath">Filepath of the database</param>
        public DatabaseContext(string databasePath)
        {
            this._databasePath = databasePath;

            //Create database file
            Database.EnsureCreated();
        }

        /// <summary>
        /// Configure filepath of database
        /// </summary>
        /// <param name="optionsBuilder">OptionsBuilder of database context</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(string.Format("Filename={0}", _databasePath));
        }
    }
}
