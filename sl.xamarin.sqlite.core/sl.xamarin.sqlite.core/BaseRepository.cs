﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace sl.xamarin.sqlite.core
{
    public abstract class BaseRepository<T>
    {
        //Database context
        protected readonly DatabaseContext DatabaseContext;

        /// <summary>
        /// Base constructor of the database Repositories.
        /// Controls the access of the different database tables.
        /// </summary>
        /// <param name="databasePath">Filepath of the database file</param>
        protected BaseRepository(string databasePath)
        {
            this.DatabaseContext = new DatabaseContext(databasePath);
        }

        /// <summary>
        /// Get all objects of the table.
        /// </summary>
        /// <returns>IEnumerable of the table objects</returns>
        public abstract Task<List<T>> GetObjectsAsync();

        /// <summary>
        /// Get specific object by id.
        /// </summary>
        /// <param name="id">Id of the object as integer</param>
        /// <returns>Object of the table</returns>
        public abstract Task<T> GetObjectByIdAsync(int id);

        /// <summary>
        /// Add new object to the table.
        /// </summary>
        /// <param name="t">Object of the table</param>
        /// <returns>Result of successful insert as boolean</returns>
        public abstract Task<bool> AddObjectAsync(T t);

        /// <summary>
        /// Update specific object of the table.
        /// </summary>
        /// <param name="t">Object of the table</param>
        /// <returns>Result of successful update as boolean</returns>
        public abstract Task<bool> UpdateObjectAsync(T t);

        /// <summary>
        /// Remove specific object of the table.
        /// </summary>
        /// <param name="id">Id of the object</param>
        /// <returns></returns>
        public abstract Task<bool> RemoveObjectAsync(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public abstract Task<T> QueryObjectAsync(Func<T, bool> predicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public abstract Task<IEnumerable<T>> QueryObjectsAsync(Func<T, bool> predicate); 
    }
}
